# StarDiscoWars

Exercise for IOC Android subject. Due to this, some annotations are in Catalan, some other in English. 
Resources fonts are too much to mention (search take and don't ask method).
But deserve to mention:
The main theme, StarWars Disco Version belongs to Meco
- Asteroid images delivered by IOC
- Background Image taken from Hubble images
- Milleniun Falcon Sprites taken from GameBoy Advance StarWars game. Can be found in
- https://www.spriters-resource.com/game_boy_advance/starwarsfotf/sheet/68827/
- I can't mention the authors of all other resources (mainly sounds). My apologies.
  If you are one of them, let me know and I will remove it from the app.

Feel free to share this project and continue the quest for the last disco pub in the far, far away galaxy.

...may the Funk be with you


