package cat.xtec.ioc.objects;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;


import java.util.Random;

import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.utils.Methods;
import cat.xtec.ioc.utils.Settings;

public class Asteroid extends Scrollable {

    private Circle collisionCircle;

    Random r;

    int assetAsteroid;
    //size of new asteroid
    float newSize;

    public Asteroid(float x, float y, float width, float height, float velocity) {
        super(x, y, width, height, velocity);

        // Creem el cercle
        collisionCircle = new Circle();

        /* Accions */
        r = new Random();
        assetAsteroid = r.nextInt(15);

        setOrigin();

        // Rotacio
        RotateByAction rotateAction = new RotateByAction();
        rotateAction.setAmount(-90f);
        rotateAction.setDuration(0.2f);

        // Accio de repetició
        RepeatAction repeat = new RepeatAction();
        repeat.setAction(rotateAction);
        repeat.setCount(RepeatAction.FOREVER);

        // Equivalent:
        // this.addAction(Actions.repeat(RepeatAction.FOREVER, Actions.rotateBy(-90f, 0.2f)));

        this.addAction(repeat);

    }

    public void setOrigin() {

       this.setOrigin(width/2 + 1, height/2);


    }

    @Override
    public void act(float delta) {
        super.act(delta);

        //TODO 2.2 comportment quan es tirat fora de la pantalla
        if (this.velocity > 0 && this.getX() > Settings.GAME_WIDTH){
            this.reset(Settings.GAME_WIDTH + r.nextInt(Settings.ASTEROID_GAP));
        }
        if (this.getX() > Settings.GAME_WIDTH){
                this.velocity=Settings.ASTEROID_SPEED;

            }

        // Actualitzem el cercle de col·lisions (punt central de l'asteroid i el radi.
        collisionCircle.set(position.x + width / 2.0f, position.y + width / 2.0f, (width / 2.0f)-3);

    }

    @Override
    public void reset(float newX) {
        this.velocity=Settings.ASTEROID_SPEED;

        //TODO 2.2 reset en funció de si es creat despŕes de que el predecessor haga eixit per la
        // dreta de la pantalla o si ha sigut impactat
        if (newX < Settings.GAME_WIDTH){
            newX = Settings.GAME_WIDTH+Settings.ASTEROID_GAP;}

        super.reset(newX);

        // Obtenim un número al·leatori entre MIN i MAX
        newSize = Methods.randomFloat(Settings.MIN_ASTEROID, Settings.MAX_ASTEROID);
        // Modificarem l'alçada i l'amplada segons l'al·leatori anterior
        width = height = 34 * newSize;
        // La posició serà un valor aleatòri entre 0 i l'alçada de l'aplicació menys l'alçada
        position.y =  new Random().nextInt(Settings.GAME_HEIGHT - (int) height);

        assetAsteroid = r.nextInt(15);
        setOrigin();
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(AssetManager.asteroid[assetAsteroid], position.x, position.y, this.getOriginX(),
                this.getOriginY(), width, height, this.getScaleX(), this.getScaleY(), this.getRotation());
    }

    // Retorna true si hi ha col·lisió
    public boolean collides(Spacecraft nau) {
        if (position.x <= nau.getX() + nau.getWidth()) {
            // Comprovem si han col·lisionat sempre i quan l'asteroid estigui a la mateixa alçada que la spacecraft
            return (Intersector.overlaps(collisionCircle, nau.getCollisionRect()));
        }
        return false;
    }

    //TODO 2.2 impacte del laser
    public boolean laserImpact(Laser laser){
         if (position.x <= laser.getX()) {

             return (Intersector.overlaps(collisionCircle,laser.getCollisionRect()));

         }
         return false;
    }

    public Circle getCollisionCircle(){return collisionCircle;}

    public int getType(){
        return assetAsteroid;
    }
    public float getSize(){
        return newSize;
    }

}
