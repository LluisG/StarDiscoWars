package cat.xtec.ioc.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;

//TODO EXER2.2 + EXER 4.1 Clase MyButton amb parametres de posicio, mides i dibuix
public class Mybutton extends Actor  {
    private Vector2 position;
   private int width, height;
   private TextureRegion texture;

    public Mybutton(float x, float y, int width, int height, TextureRegion texture ){

        this.texture = texture;
        this.width = width;
        this.height = height;
        position = new Vector2(x, y);
         setBounds(position.x, position.y, width, height);
        setTouchable(Touchable.enabled);

    }
    //TODO pintem el boto
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(texture, position.x, position.y, width, height);
    }

    public void setVisible(Boolean bol){
        this.setVisible(bol);
    }
     public void act(float delta) {
        super.act(delta);}


    public float getX() {
        return position.x;
    }
    public float getY() {
        return position.y;
    }
    public float getWidth() {
        return width;
    }
    public float getHeight() {
        return height;
    }

}


