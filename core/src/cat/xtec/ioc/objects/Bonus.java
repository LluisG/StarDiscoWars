package cat.xtec.ioc.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;

import java.util.Random;

import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.utils.Settings;

//TODO 4 Clase Bonus amb el arguments de possició, tamany, velocitat, dibuix i valor.
public class Bonus extends Scrollable{

    Boolean colliding = false;
    Random r;
    private Rectangle collisionRect;
    private int puntos;
    private TextureRegion textureregion;


    public Bonus(float x, float y, float width, float height, float velocity, int punts,
                 TextureRegion textureRegion ) {
        super(x, y, width, height, velocity);
        textureregion = textureRegion;

        puntos = punts;
        position = new Vector2(x, y);
        r = new Random();
        collisionRect = new Rectangle();



    }
    //TODO 3 actualitzem la possicio
    @Override
    public void act(float delta) {
        super.act(delta);

        collisionRect.set(position.x , position.y , 17, 35);
    }
    //TODO 3 creem un nou bonus
    @Override
    public void reset(float newX) {
        super.reset(newX);
        colliding=false;
        position.x = Settings.GAME_WIDTH;
        position.y =  new Random().nextInt(Settings.GAME_HEIGHT - (int) height);
    }

    //TODO 3 pintem els bonus
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(textureregion, position.x,position.y, width, height);
    }

        //todo mirem si el bonus ha col·lisionat emb la nau
    public boolean collides(Spacecraft nau) {
        if (position.x <= nau.getX() && !colliding ) {
            colliding = true;

            return (Intersector.overlaps(collisionRect, nau.getCollisionRect()));
        }
            return false;
    }
    }

