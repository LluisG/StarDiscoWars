package cat.xtec.ioc.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;

import cat.xtec.ioc.helpers.AssetManager;


//TODO EXER 2.1 Clase laser amb parametres de tamnay, velocitat i posicio
public class Laser extends Scrollable {

    boolean colliding = false;

    private Rectangle collisionRect;

    public Laser(float x, float y, float width, float height, float velocity) {
        super(x, y, width, height, velocity);

        collisionRect = new Rectangle();

    }
//TODO EXER 2.1 cambien la posicio del laser
    @Override
    public void reset(float newY) {
        super.reset(newY);
        position.y = newY;
        colliding = false;

    }
    //TODO EXER 2 pintem el laser
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(AssetManager.laser, position.x, position.y,width, height);
    }

    public void act(float delta) {
        super.act(delta);
        collisionRect.set(position.x+2, position.y+2, 17, 10);


    }
    //TODO exer2 retornem el rectangle de col·lisio
    public Rectangle getCollisionRect() {
        return collisionRect;
    }

}



