package cat.xtec.ioc.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;

import java.util.ArrayList;
import java.util.Random;

import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.screens.GameScreen;
import cat.xtec.ioc.utils.Methods;
import cat.xtec.ioc.utils.Settings;


public class ScrollHandler extends Group {

    // Fons de pantalla
    Background bg, bg_back;

    // Asteroides
    int numAsteroids;
    private ArrayList<Asteroid> asteroids;

    //3.1 instancia dels bonus
    private Bonus bonus1;
    private Bonus bonus2;
    //2.2 instacien del feix
    private Laser laser;

//EXER 2.1 Instancia del boto de fire
private Mybutton firebutton;

// EXER  4.1 Instancia del boto de pause
private Mybutton pauseButton;

//BackButton
    private Mybutton exitButton;

    private Mybutton goButton;

    // Objecte Random
    Random r;

    // exer 2.2 boolea per a que compte només el 1er impacte del laser

    public boolean hitting = false;


    public ScrollHandler() {

        // Creem els dos fons
        bg = new Background(0, 0, Settings.GAME_WIDTH * 2, Settings.GAME_HEIGHT, Settings.BG_SPEED);
        bg_back = new Background(bg.getTailX(), 0, Settings.GAME_WIDTH * 2, Settings.GAME_HEIGHT, Settings.BG_SPEED);

        // Afegim els fons al grup
        addActor(bg);
        addActor(bg_back);

        // Creem l'objecte random
        r = new Random();

        // Comencem amb 3 asteroids
        numAsteroids = 3;

        // EXER 2.2 boto per al laser
        firebutton = new Mybutton(200, 80,25, 25, AssetManager.fireButton);
        addActor(firebutton);
        firebutton.setName("firebutton");

        // EXER 4.1 Pause BUtton
        pauseButton= new Mybutton(Settings.GAME_WIDTH-30, 2,25, 25, AssetManager.pauseButton);
        addActor(pauseButton);
        pauseButton.setName("pauseButton");

        //BackButton
        exitButton = new Mybutton(2, 2, 25, 25, AssetManager.exitButton);
        addActor(exitButton);
        exitButton.setName("backButton");
        exitButton.setVisible(false);

        goButton = new Mybutton(200, 80, 25, 25, AssetManager.arrowGoButton);
        goButton.setVisible(false);
        goButton.setName("goButton");
        addActor(goButton);





        // exer 3.2 instancies dels ninos del bonus
        // exer 1.2 elemts del sheet propi
        bonus1 = new Bonus(Settings.GAME_WIDTH,r.nextInt(Settings.GAME_HEIGHT),17,
                35,Settings.SCORE1_SPEED,Settings.SCORE1_INCREASE,AssetManager.bonus1);

        addActor(bonus1);
        bonus2 = new Bonus(Settings.GAME_WIDTH,r.nextInt(Settings.GAME_HEIGHT),17,
                35,Settings.SCORE2_SPEED,Settings.SCORE2_INCREASE, AssetManager.bonus2);
        addActor(bonus2);


        // exer 2.1 creem el laser beam
        laser = new Laser(0,Settings.GAME_HEIGHT/2f,30,10, Settings.VELOCITAT_LASER);
        addActor(laser);


        // Creem l'ArrayList
        asteroids = new ArrayList<Asteroid>();

        // Definim una mida aleatòria entre el mínim i el màxim
        float newSize = Methods.randomFloat(Settings.MIN_ASTEROID, Settings.MAX_ASTEROID) * 34;

        // Afegim el primer Asteroid a l'Array i al grup
        Asteroid asteroid = new Asteroid(Settings.GAME_WIDTH, r.nextInt(Settings.GAME_HEIGHT - (int) newSize), newSize, newSize, Settings.ASTEROID_SPEED);
        asteroids.add(asteroid);
        addActor(asteroid);



        // Des del segon fins l'últim asteroide
        for (int i = 1; i < numAsteroids; i++) {
            // Creem la mida al·leatòria
            newSize = Methods.randomFloat(Settings.MIN_ASTEROID, Settings.MAX_ASTEROID) * 34;
            // Afegim l'asteroid.
            asteroid = new Asteroid(asteroids.get(asteroids.size() - 1).getTailX() +
                    Settings.ASTEROID_GAP, r.nextInt(Settings.GAME_HEIGHT - (int) newSize),
                    newSize, newSize, Settings.ASTEROID_SPEED);
            // Afegim l'asteroide a l'ArrayList
            asteroids.add(asteroid);
            // Afegim l'asteroide al grup d'actors
            addActor(asteroid);
        }

    }

    @Override
    public void act(float delta) {

        super.act(delta);
        // Si algun element està fora de la pantalla, fem un reset de l'element.
        if (bg.isLeftOfScreen()) {
            bg.reset(bg_back.getTailX());

        } else if (bg_back.isLeftOfScreen()) {
            bg_back.reset(bg.getTailX());

        }

        for (int i = 0; i < asteroids.size(); i++) {

            Asteroid asteroid = asteroids.get(i);
            if (asteroid.isLeftOfScreen()) {
                if (i == 0) {
                    asteroid.reset(asteroids.get(asteroids.size() - 1).getTailX() + Settings.ASTEROID_GAP);
                    // EXER 3.2 / probabilitat bonus

                    int f = r.nextInt(100);
                    if(f<50){
                        bonus1.reset(Settings.GAME_WIDTH);
                    }if(f<10){
                        bonus2.reset(Settings.GAME_WIDTH);}
                } else {

                    asteroid.reset(asteroids.get(i - 1).getTailX() + Settings.ASTEROID_GAP);
                }
            }
        }
    }

   /*remove for testing*/
    public boolean collides(Spacecraft nau) {

        // Comprovem les col·lisions entre cada asteroid i la nau
        for (Asteroid asteroid : asteroids) {
            if (asteroid.collides(nau)) {
                return true;
            }
        }
            return false;

    }
    // 2.2 fire laser
    public void fireLaser(Spacecraft nau) {


        if (laser.getX() > Settings.GAME_WIDTH) {
            laser.setVisible(true);
            hitting=false;
            AssetManager.blaster.play(0.2f);
            laser.reset(nau.getY());
        }


    }

  // 2.2 el laser impacta

    public boolean laserImpact(Asteroid asteroid){
            if (asteroid.laserImpact(laser) && !hitting){
                AssetManager.asteroidLaser.play(0.5f);
                laser.setVisible(false);
                asteroid.velocity=250;
                hitting=true;
                return true;
            }
        return false;

    }


    // 3.1 agafem els dos bonus
    public boolean bonus1Collides(Spacecraft nau) {
        if (bonus1.collides(nau) ) {

        return true;
        }
         return false;
    }

    public boolean bonus2Collides(Spacecraft nau) {
        if (bonus2.collides(nau) ) {

        return true;
        }
        return false;
    }


    public void reset() {
        // Posem el primer asteroid fora de la pantalla per la dreta
        asteroids.get(0).reset(Settings.GAME_WIDTH);
        // Calculem les noves posicions de la resta d'asteroids.
        for (int i = 1; i < asteroids.size(); i++) {
            asteroids.get(i).reset(asteroids.get(i - 1).getTailX() + Settings.ASTEROID_GAP);
        }
    }

    public ArrayList<Asteroid> getAsteroids() {
        return asteroids;
    }

     // EXER 2.2 retorne el FireButton
    public Mybutton getFirebutton() {
        return firebutton;
    }

// EXER 2.2 retorne el PauseBUtton
    public Mybutton getPauseButton(){
        return pauseButton;
    }

    //return backButton
    public Mybutton getExitButton(){return exitButton;}

    //return goButton
    public Mybutton getGoButton(){return goButton;}
}