package cat.xtec.ioc.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.helpers.InputHandler;
import cat.xtec.ioc.objects.Asteroid;
import cat.xtec.ioc.objects.Laser;
import cat.xtec.ioc.objects.Mybutton;
import cat.xtec.ioc.objects.ScrollHandler;
import cat.xtec.ioc.objects.Spacecraft;
import cat.xtec.ioc.utils.Settings;





public class GameScreen implements Screen {






    //TODO EXER 4.1 Boolean de pause
    public boolean paused=false;


    private SplashScreen splashScreen;


    private Label textLbl;
    private int puntuacio = 0;

    private Label.LabelStyle textStyle;


    // Els estats del joc

    public enum GameState {

        READY, RUNNING, GAMEOVER

    }

    private GameState currentState;

    // Objectes necessaris
    private Stage stage;
    private Spacecraft spacecraft;
    private ScrollHandler scrollHandler;

    //FireButton out of scree when dead
    private Mybutton FB;

    //pauseButton out of screen when dead
    private Mybutton PB;
    //goBUtton visible when dead
    private Mybutton GB;




    // Encarregats de dibuixar elements per pantalla
    private ShapeRenderer shapeRenderer;
    private Batch batch;

    // Per controlar l'animació de l'explosió
    private float explosionTime = 0;

    // Preparem el textLayout per escriure text
    private GlyphLayout textLayout;

    //TODO exer 2.2 boolea per a saber si el laser esta disparat o no
    public boolean laserFired = false;
    //TODO EXER 5 instancia re Preferencees per a guardar-recuperar la puntuacio
    private Preferences record;

    public GameScreen(Batch prevBatch, Viewport prevViewport) {

        //TODO EXER 5 Codi de les nostres dades
        record = Gdx.app.getPreferences("millorPartida");

        // Creem el ShapeRenderer
        shapeRenderer = new ShapeRenderer();

        // Creem l'stage i assginem el viewport
        stage = new Stage(prevViewport, prevBatch);

        //parem la musica de la intro
        AssetManager.intro.stop();
        // Iniciem la música
        AssetManager.music.play();


        batch = stage.getBatch();

        // Creem la nau i la resta d'objectes
        spacecraft = new Spacecraft(Settings.SPACECRAFT_STARTX, Settings.SPACECRAFT_STARTY,
                Settings.SPACECRAFT_WIDTH, Settings.SPACECRAFT_HEIGHT);

        scrollHandler = new ScrollHandler();

        PB = scrollHandler.getPauseButton();
        FB = scrollHandler.getFirebutton();
        GB = scrollHandler.getGoButton();


        // Afegim els actors a l'stage
        stage.addActor(scrollHandler);

        stage.addActor(spacecraft);
        // Donem nom a l'Actor
        spacecraft.setName("spacecraft");

        // Iniciem el GlyphLayout
        textLayout = new GlyphLayout();
        textLayout.setText(AssetManager.font, "...noto una perturbacion \n  en la pista");

        currentState = GameState.READY;

        // Assignem com a gestor d'entrada la classe InputHandler
        Gdx.input.setInputProcessor(new InputHandler(this));

        //TODO Contador al mig de la pantalla
         textStyle = new Label.LabelStyle(AssetManager.font, Color.YELLOW);
         textLbl = new Label(String.valueOf(puntuacio), textStyle);
         textLbl.setPosition(Settings.GAME_WIDTH/2f, 3);
         stage.addActor(textLbl);

    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

         //TODO 4.1 anem a pausa reduint el delta a 0
        if (paused){
            delta=0;
            //TODO 4.1 baixem el vol x10(les sensacions fisiques tenen relacio logaritmica
            // amb la intensitat de l’estimul)
            AssetManager.music.setVolume(0.05f);
        }

        // Dibuixem tots els actors de l'stage
        stage.draw();

        // Depenent de l'estat del joc farem unes accions o unes altres
        switch (currentState) {

            case GAMEOVER:
                updateGameOver(delta);
                break;
            case RUNNING:
                updateRunning(delta);
                break;
            case READY:
                updateReady();
                break;


        }

        //drawElements();

    }

    private void updateReady() {

        // Dibuixem el text al centre de la pantalla
        batch.begin();
        AssetManager.font.draw(batch, textLayout, (Settings.GAME_WIDTH / 2f) - textLayout.width
                / 2, (Settings.GAME_HEIGHT / 2f) - textLayout.height / 2);
        //stage.addActor(textLbl);
        batch.end();

    }

    private void updateRunning(float delta) {

        stage.act(delta);


        //TODO 2.2 laser disparat

        if (laserFired){
            scrollHandler.fireLaser(spacecraft);
            laserFired=false;
        }

        // 2.2 laser impacta asteroid/Disco Item
        for (Asteroid asteroid : scrollHandler.getAsteroids()) {
        if (scrollHandler.laserImpact(asteroid)){

            int n = asteroid.getType();
            Gdx.app.log("AsteroidTYpe", String.valueOf(n));
                if(n==0 || n==1 || n==2  || n==4 || n==7){
                    int pointsToAdd = (int) ((1.6f-asteroid.getSize())*10);
                    Gdx.app.log("Size", String.valueOf(asteroid.getSize()));

                  puntuacio += pointsToAdd;
                    Gdx.app.log("Points", String.valueOf(pointsToAdd));
                     textLbl.setText(puntuacio);
                }


       }
        }

        //  3.1 Bonus

        if (scrollHandler.bonus1Collides(spacecraft)) {
            AssetManager.han.play(0.5f);
            puntuacio += Settings.SCORE1_INCREASE;
            textLbl.setText(puntuacio);
        }

        if (scrollHandler.bonus2Collides(spacecraft)) {
            AssetManager.ouyeahSound.play(0.5f);
            puntuacio += Settings.SCORE2_INCREASE;
            textLbl.setText(puntuacio);
        }


        if (scrollHandler.collides(spacecraft)) {
            // Si hi ha hagut col·lisió: Reproduïm l'explosió i posem l'estat a GameOver
            AssetManager.explosionSound.play();
            AssetManager.chuiSound.play();
            stage.getRoot().findActor("spacecraft").remove();


            String missatge;
              // 5.1 recuperem el record
            int maximRecord= record.getInteger("millorPartida");
            String StringRecord="Record actual: "+ maximRecord +"\n";

             // 5.1 comparem la puntuacio amb el record. Si > sobreescribim el valor i pintem
            // el missatge adient
            if (puntuacio>maximRecord){
                StringRecord = "Nuevo Record!!\n";
                record.putInteger("millorPartida", puntuacio);
                record.flush();
            }
              //5.1 Construim String en funcio de la puntuacio
                if (puntuacio<100){
                missatge = ("\n Tu carencia de Flow \n resulta molesta... ");
            }else {
                if (100<=puntuacio && puntuacio <=150){
                    missatge = ("\n El Flow es poderoso \n en ti...");
                }else{
                    missatge=("\n ...yo soy tu papi ");
                }
            }
            // 5.1 pintem el missatge
            textLayout.setText(AssetManager.font,(StringRecord+"Puntos: "+ puntuacio + missatge));

            currentState = GameState.GAMEOVER;


        }

    }

    private void updateGameOver(float delta) {
        stage.act(delta);

        batch.begin();
        AssetManager.font.draw(batch, textLayout, (Settings.GAME_WIDTH - textLayout.width) / 2,
                (Settings.GAME_HEIGHT - textLayout.height) / 2);
        // Si hi ha hagut col·lisió: Reproduïm l'explosió
        batch.draw((TextureRegion) AssetManager.explosionAnim.getKeyFrame(explosionTime, false),
                (spacecraft.getX() + spacecraft.getWidth() / 2) - 32, spacecraft.getY() +
                        spacecraft.getHeight() / 2 - 32, 64, 64);
        batch.end();

        //FireButton out of screen when dead

        FB.setVisible(false);
        //pauseButton out of screen when dead
        PB.setVisible(false);
        //goButton visible when dead
        GB.setVisible(true);
        explosionTime += delta;

    }

    public void reset() {
        PB.setVisible(true);
        FB.setVisible(true);
        GB.setVisible(false);
        // Posem el text d'inici
        textLayout.setText(AssetManager.font, "que el Funk sea contigo...");
        // Cridem als restart dels elements.
        spacecraft.reset();
        scrollHandler.reset();

        // Posem l'estat a 'Ready'
        currentState = GameState.READY;

        // Afegim la nau a l'stage
        stage.addActor(spacecraft);
        puntuacio =0;
        textLbl.setText(puntuacio);


        // Posem a 0 les variables per controlar el temps jugat i l'animació de l'explosió
        explosionTime = 0.0f;

    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {


    }


    @Override
    public void resume() {
      //  AssetManager.music.setVolume(1);


    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public Spacecraft getSpacecraft() {
        return spacecraft;
    }

    public Stage getStage() {
        return stage;
    }


    public ScrollHandler getScrollHandler() {
        return scrollHandler;
    }

    public GameState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(GameState currentState) {
        this.currentState = currentState;
    }




}
