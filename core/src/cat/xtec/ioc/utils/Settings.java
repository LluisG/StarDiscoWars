package cat.xtec.ioc.utils;

public class Settings {

    // Mida del joc, s'escalarà segons la necessitat
    public static final int GAME_WIDTH = 240;
    public static final int GAME_HEIGHT = 135;

    // Propietats de la nau
    public static final float SPACECRAFT_VELOCITY = 85;
    public static final int SPACECRAFT_WIDTH = 36;
    public static final int SPACECRAFT_HEIGHT = 15;
    public static final float SPACECRAFT_STARTX = 20;
    public static final float SPACECRAFT_STARTY = GAME_HEIGHT/2f - SPACECRAFT_HEIGHT/2f;

    // Rang de valors per canviar la mida de l'asteroide.
    public static final float MAX_ASTEROID = 1.5f;
    public static final float MIN_ASTEROID = 0.5f;

    // Configuració Scrollable
    public static final int ASTEROID_SPEED = -150;
    public static final int ASTEROID_GAP = 75;
    public static final int BG_SPEED = -20;

    // TODO Exercici 2: Propietats per la moneda
    public static final int SCORE1_INCREASE = 15; // s'incrementa en 10 cada cop que toca una moneda
    public static final int SCORE1_SPEED = -175;

    public static final int SCORE2_INCREASE = 50; // s'incrementa en 50 cada cop que toca una moneda
    public static final int SCORE2_SPEED = -250;


    public static final int VELOCITAT_LASER = 350;

}
