package cat.xtec.ioc.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetManager {

    // Sprite Sheet
    public static Texture sheet;

    // Nau i fons
    //TODO  1.2 MOdificacions per a carregar les noves imatges
    public static TextureRegion spacecraft, spacecraftDown, spacecraftUp, background, laser,
            bonus1,bonus2, fireButton, pauseButton,exitButton,arrowGoButton;

    // Asteroid
    public static TextureRegion[] asteroid;
    public static Animation asteroidAnim;

    // Explosió
    public static TextureRegion[] explosion;
    public static Animation explosionAnim;

    // Sons
    public static Sound explosionSound;
    public static Music music;
    public static Music intro;


     public static Sound chuiSound;
     public static Sound han;
     public static Sound blaster;

     public static Sound ouyeahSound;
     public static Sound asteroidLaser;

    // Font
      public static BitmapFont font;



    public static void load() {
        // Carreguem les textures i li apliquem el mètode d'escalat 'nearest'
        sheet = new Texture(Gdx.files.internal("sheet4.png"));
        sheet.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        // Sprites de la nau
        spacecraft = new TextureRegion(sheet, 1, 1, 34, 14);
        spacecraft.flip(false, true);

        spacecraftUp = new TextureRegion(sheet, 37, 1, 34, 14);
        spacecraftUp.flip(false, true);

        spacecraftDown = new TextureRegion(sheet, 73, 1, 34, 14);
        spacecraftDown.flip(false, true);



        // Carreguem els 16 estats de l'asteroid
        asteroid = new TextureRegion[16];
        for (int i = 0; i < asteroid.length; i++) {

            asteroid[i] = new TextureRegion(sheet, i * 34, 15, 34, 34);
            //  asteroid[i].flip(false, true);



        }

        //TODO  1.2 Modificacions per a carregar les noves imatges
        laser = new TextureRegion(sheet,487,175,50,9);

        //TODO Fire/PauseBUtton
        fireButton = new TextureRegion(sheet,488,228,25,25);
        pauseButton=new TextureRegion(sheet,489,253, 21,21);
        exitButton = new TextureRegion(sheet,488,277,23,23);

        arrowGoButton = new TextureRegion(sheet,514,276,25,25);

        //TODO 3.2 Ninos bonus
        bonus1 = new TextureRegion(sheet,486,189,17,35);
        bonus1.flip(false, true);

        bonus2 = new TextureRegion(sheet,507,189,17,35);
        bonus2.flip(false, true);



        // Creem l'animació de l'asteroid i fem que s'executi contínuament en sentit anti-horari
        asteroidAnim = new Animation(0.05f, asteroid);
        asteroidAnim.setPlayMode(Animation.PlayMode.LOOP_REVERSED);

        // Creem els 16 estats de l'explosió
        explosion = new TextureRegion[16];

        // Carreguem els 16 estats de l'explosió
        int index = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 8; j++) {
                explosion[index++] = new TextureRegion(sheet, j * 64,  i * 64 + 49, 64, 64);
              //  explosion[index-1].flip(false, true);
            }
        }

        // Finalment creem l'animació
        explosionAnim = new Animation(0.04f, explosion);

        // Fons de pantalla
        background = new TextureRegion(sheet, 0, 177, 480, 135);
    //    background.flip(false, true);

        /******************************* Sounds *************************************/
        // Explosió
        explosionSound = Gdx.audio.newSound(Gdx.files.internal("sounds/explosion.wav"));

        chuiSound = Gdx.audio.newSound(Gdx.files.internal("sounds/ChewbaccaRoar.wav"));


        ouyeahSound = Gdx.audio.newSound(Gdx.files.internal("sounds/oyeah.ogg"));

        // Música del joc
        music = Gdx.audio.newMusic(Gdx.files.internal("sounds/discoWars.mp3"));
        music.setVolume(0.2f);
        music.setLooping(true);

        han=Gdx.audio.newSound(Gdx.files.internal("sounds/han.ogg"));

        blaster = Gdx.audio.newSound(Gdx.files.internal("sounds/blaster.mp3"));
        asteroidLaser = Gdx.audio.newSound(Gdx.files.internal("sounds/asteroidExplode.mp3"));

        intro = Gdx.audio.newMusic(Gdx.files.internal("sounds/intro.ogg"));
        intro.setVolume((0.4f));
        intro.setLooping(true);

        /******************************* Text *************************************/
        // Font space
//        FileHandle fontFile = Gdx.files.internal("fonts/space.fnt.fnt");
        FileHandle fontFile = Gdx.files.internal("fonts/starDiscoWarsFont.fnt");
        font = new BitmapFont(fontFile, true);
        font.setColor(Color.YELLOW);
        font.getData().setScale(0.4f);


    }



    public static void dispose() {

        // Alliberem els recursos gràfics i de audio
        sheet.dispose();
        explosionSound.dispose();
        music.dispose();


    }
}
