package cat.xtec.ioc.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;

import cat.xtec.ioc.objects.Mybutton;
import cat.xtec.ioc.objects.Spacecraft;
import cat.xtec.ioc.screens.GameScreen;


public class InputHandler implements InputProcessor {

    // Enter per a la gesitó del moviment d'arrastrar
    int previousY = 0;
    // Objectes necessaris
    private Spacecraft spacecraft;
    private GameScreen screen;
    private Vector2 stageCoord;
    // 2.2 instancia del boto del laser
    private Mybutton firebutton;
    // 4.1 instancia del boto de pause
    private Mybutton pauseButton;

    private Mybutton goButton;

    //backButton
    private Mybutton exitButton;


    private Stage stage;

    //////////////////////////////
    private Actor actorHit;

    /////////////////
    public InputHandler(GameScreen screen) {

        /////////////////////
       // stageCoord = stage.screenToStageCoordinates(new Vector2(screenX, screenY));
//        Gdx.app.log("HIT", stageCoord.toString());
//        actorHit = stage.hit(stageCoord.x, stageCoord.y, true);




        // Obtenim tots els elements necessaris
        this.screen = screen;
        spacecraft = screen.getSpacecraft();
        stage = screen.getStage();

        //2.2 delaracio del boto del laser
        firebutton = screen.getScrollHandler().getFirebutton();
        //4.1 delaracio del boto del pause
        pauseButton=screen.getScrollHandler().getPauseButton();
        //back Button declaration
        exitButton=screen.getScrollHandler().getExitButton();

        //goButton

        goButton=screen.getScrollHandler().getGoButton();

    }


    @Override
    public boolean keyDown(int keycode) {return false;}

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        stageCoord = stage.screenToStageCoordinates(new Vector2(screenX, screenY));
      actorHit = stage.hit(stageCoord.x, stageCoord.y, true);


        switch (screen.getCurrentState()) {
            case READY:

                // Si fem clic comencem el joc
                screen.setCurrentState(GameScreen.GameState.RUNNING);
                break;
            case RUNNING:
                previousY = screenY;


                // 4.1 tornem de pausa si piquem a la pantalla
                if (actorHit == null) {

                    Gdx.app.log("PAUSED", String.valueOf(screen.paused));
                    if (screen.paused) {
                        screen.paused=false;
                        pauseButton.setVisible(true);
                        firebutton.setVisible(true);
                        exitButton.setVisible(false);
                        AssetManager.music.setVolume(0.2f);
                        //screen.pa

                    }

                }
                //2.2 Disparem el laser
                if (actorHit == firebutton) {
                    screen.laserFired = true;

                }

                //ff paused, go back to SplashScreen
                if (actorHit == exitButton ){
                    //screen.setCurrentState(GameScreen.GameState.READY);
                    Gdx.app.log("HIT", stageCoord.toString());
                    //screen.setSplashScreen();
                    Gdx.app.exit();

                }

                //4.1 anem a pausa piquen el boto de pausa
                if (actorHit == pauseButton){
                        Gdx.app.log("HIT", "pause");
                        pauseButton.setVisible(false);
                        firebutton.setVisible((false));
                        exitButton.setVisible(true);
                        screen.paused=true;

                    }

                break;
            // Si l'estat és GameOver tornem a iniciar el joc
            case GAMEOVER:
                  Gdx.app.log("Sate", "Gameover");
                  if (actorHit == goButton){
                screen.reset();}
                break;
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        // Quan deixem anar el dit acabem un moviment
        // i posem la nau en l'estat normal
        spacecraft.goStraight();
        return true;
    }


    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // Posem un umbral per evitar gestionar events quan el dit està quiet
        if (Math.abs(previousY - screenY) > 2)

            // Si la Y és major que la que tenim
            // guardada és que va cap avall
            if (previousY < screenY) {
                spacecraft.goDown();
            } else {
                // En cas contrari cap a dalt
                spacecraft.goUp();
            }
        // Guardem la posició de la Y
        previousY = screenY;
        return true;


    }



    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


}
